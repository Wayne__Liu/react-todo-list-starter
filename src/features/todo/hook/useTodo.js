import * as todoApi from "../../../apis/todo"
import { useDispatch } from "react-redux";
import { initTodoTask } from "../reducers/todoSlice";


export const useTodo = () => {
    const dispatch = useDispatch();
    async function loadTodo() {
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTask(response.data));
    }
    const updateTodo = async (id, todoTask) => {
        await todoApi.updateTodoTasks(id, todoTask);
        await loadTodo();
    }
    const deleteTodo = async (id) => {
        await todoApi.deleteTodoTasks(id);
        await loadTodo();
    }
    const createTodo = async (todoTask) => {
        await todoApi.createTodoTasks(todoTask);
        await loadTodo();
    }
    const getTodoById = async (id) => {
        await todoApi.getTodoByIdTasks(id);
    }

    return { loadTodo, updateTodo, deleteTodo, createTodo, getTodoById };
}