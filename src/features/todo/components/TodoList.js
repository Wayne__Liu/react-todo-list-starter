import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { useEffect } from "react";
import { useTodo } from "../hook/useTodo";

export default function TodoList() {

    const { loadTodo } = useTodo();
    // const dispatch = useDispatch();
    useEffect(() => {
        loadTodo()
    }, [])

    return (
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
}