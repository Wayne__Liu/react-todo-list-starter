import { useState } from "react";
import { useTodo } from "../hook/useTodo";
import { Button, Input } from 'antd';

export default function TodoGenerator() {
    const [taskName, setTaskName] = useState("");
    const { createTodo } = useTodo();

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask = async () => {
        await createTodo({ name: taskName, done: false })
        setTaskName("");
    }
    return (
        <div className='todo-generator'>
            <Input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></Input>
            <Button onClick={handleAddTodoTask} disabled={!taskName} > ADD </Button>
        </div>
    );
}