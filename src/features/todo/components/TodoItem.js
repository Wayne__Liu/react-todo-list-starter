import { useTodo } from "../hook/useTodo";
import React, { useState, useEffect } from 'react';
import { Button, Modal, Input, Tooltip } from 'antd';
export default function TodoItem({ task }) {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isDisplayModalOpen, setIsDisplayModalOpen] = useState(false);
    const [inputValue, setInputValue] = useState('');
    useEffect(() => {
        setInputValue(task.name);
    }, [task]);
    const handleOk = () => {
        if (!inputValue) {
            return;
        }
        setIsModalOpen(false);
        updateTodo(task.id, { name: inputValue })
        setInputValue('');
    };
    const handleCancel = () => {
        setIsModalOpen(false);
        setInputValue(task.name);
    };
    const handleDisplayOk = () => {
        setIsDisplayModalOpen(false);
    };
    const handleDisplayCancel = () => {
        setIsDisplayModalOpen(false);
    };
    const { updateTodo, deleteTodo, getTodoById } = useTodo();
    const handleTaskNameClick = async () => {
        await updateTodo(task.id, { done: !task.done })
    }
    const hanlleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            await deleteTodo(task.id)
        }
    }
    const hanlleUpdateButtonClick = async () => {
        setIsModalOpen(true);
    }
    const hanlleDisplayButtonClick = async () => {
        await getTodoById(task.id)
        setIsDisplayModalOpen(true);
    }
    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    };
    const truncatedName = task.name.length > 32 ? task.name.slice(0, 32) + '...' : task.name;

    return (
        <div className='button-container'>
            <div className={`task-name ${task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>
                {task.name.length > 32 ? (
                    <Tooltip title={task.name}>
                        {truncatedName}
                    </Tooltip>
                ) : (
                    truncatedName
                )}
            </div>
            <div className="todo-item">
                <Button className="todo-item-px" onClick={hanlleDisplayButtonClick}>㊙️</Button >
                <Button className="todo-item-px" onClick={hanlleUpdateButtonClick}>🖋</Button >
                <Button onClick={hanlleRemoveButtonClick}>❌</Button >
            </div>
            <Modal title="Update Todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Input placeholder="What do you want to change it to?" onChange={handleInputChange} value={inputValue} />
            </Modal>
            <Modal title="Display Todo" open={isDisplayModalOpen} onOk={handleDisplayOk} onCancel={handleDisplayCancel}>
                <Input onChange={handleInputChange} value={task.name} />
            </Modal>
        </div>
    );
}