import { NavLink, Outlet } from 'react-router-dom';
import './App.css';
import { Menu } from "antd";

function App() {
  return (
    <div className="app">
      <div className='nav-bar'>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["home"]} className="custom-menu">
          <Menu.Item key="home">
            <NavLink to="/">Home</NavLink>
          </Menu.Item>
          <Menu.Item key="done">
            <NavLink to="/done">Done List</NavLink>
          </Menu.Item>
          <Menu.Item key="help">
            <NavLink to="/help">Help</NavLink>
          </Menu.Item>
        </Menu>
      </div>
      <Outlet></Outlet>
    </div>
  );
}
export default App;
