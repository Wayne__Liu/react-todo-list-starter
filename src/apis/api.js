import axios from "axios";

const api = axios.create({
    // baseURL: "https://64c0b6680d8e251fd11263ea.mockapi.io/api/v1/"
    baseURL: "http://localhost:8080"
});

export default api;