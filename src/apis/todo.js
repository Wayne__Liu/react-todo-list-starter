import api from "./api";

export const getTodoTasks = () => {
    return api.get(`/todo`);
}

export const updateTodoTasks = (id, todoTask) => {
    return api.put(`/todo/${id}`, todoTask);
}

export const deleteTodoTasks = (id) => {
    return api.delete(`/todo/${id}`);
}

export const createTodoTasks = (todoTask) => {
    return api.post(`/todo`, todoTask);
}

export const getTodoByIdTasks = (id) => {
    return api.get(`/todo/${id}`);
}