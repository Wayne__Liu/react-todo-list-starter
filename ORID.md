## **O**：

-  MockAPI is a tool that helps front-end developers simulate back-end services. It can help us conduct testing faster during the development process and avoid development interruptions caused by unavailability of backend services.
-  React router is a library used to manage routing in React applications. It can help us create different pages in the application and switch pages based on user actions. Optimized the todolist application I wrote yesterday.
-  Axios is a library used for HTTP requests on both the front-end and back-end. It can help us send and receive data in a simple way.
-  We used the Mock API to simulate backend services and used React router to manage routing. Use Axios to send and receive data, and use Ant Design to optimize styles.



## **R：**

- pain


## **I：**

- I have learned a lot today, and my understanding of MockAPI and React router is not very deep. I need to spend more time learning. In addition, I also need to learn how to use Axios to send and receive data, as well as how to use Ant Design to optimize styles. I believe that through continuous learning and practice, I will quickly master this knowledge.


## **D：**

- Optimizing the UI or style of a page is really painful